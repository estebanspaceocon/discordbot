#!/bin/bash
#cd /home/nihal/Documents/App/Tools/discordbot
cd /home/pi/Desktop/discordbot

#git pull | grep -q 'Unpacking objects' 
#echo passed
#if [ $? = 0 ] #grep found given string
#then
#	:  #TODO: nothing
#else
#	git pull
#	pkill -f nodejs
#	nodejs bot.js
#	echo git pulled 
#fi
git fetch origin
reslog=$(git log HEAD..origin/master --oneline)
if [ "${reslog}" != "" ] ; then
  git pull # completing the pull
  pkill -f nodejs
  nodejs bot.js
fi
